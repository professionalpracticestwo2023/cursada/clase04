### A continuacion se deja una actividad para realizar en forma individual
### El objetivo es poder realizar la lectura del archivo que se suministra, con el diseño que se deja como detalle
### Para ello usaran una funcion de php que es substr()

	-  substr(string $string, int $start, int $length = ?): string

	- Devuelve una parte del string definida por los parámetros start y length. 

### Primero antes de comenzar deberán plantear un nuevo diseño de archivos pero relacional
### Una vez definido podrán empezar a armar el/los archivos a partir de la lectura del archivo principal


### Definiciones de columnas
## Columna	Nombre	Descripción	Longitud exacta	
1	Distrito	Distrito	3	
2	Chapa	Chapa	8	
3	Modelo-Año	Modelo-Año	4	
4	Codigo marca	Codigo marca	7	
5	Tipo	Tipo	2	
6	Categoria	Categoria	2	
7	Inciso	Inciso	1	
8	Base Imponible	Base Imponible	13	
9	Valuacion seguro	Valuacion seguro	13	
10	Distrito	Distrito	3	
11	Dest Postal	Dest postal	30	
12	Codigo Postal	Codigo Postal	9	
13	Localidad	Localidad	30	
14	Calle	Calle	35	
15	Numero	Numero	5	
16	Piso	Piso	3	
17	Depto	Depto	4	
18	Titular1	Titular1	30	
19	Cuit1	Cuit1	11	
20	Titular2	Titular2	30	
21	Cuit2	Cuit2	11	
22	Deuda	Deuda	13	
Total:	267	